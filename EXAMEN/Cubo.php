<?php
class Cubo{


    private $ancho;
    private $largo;
    private $alto; 

    //Constructor
    public function __construct(float $ancho, float $largo, float $alto){
        $this->ancho = $ancho;
        $this->largo = $largo;
        $this->alto = $alto;

    }

    public function mostrar(): string{
        return "Alto:  $this->alto, Ancho: $this->ancho, Largo: $this->largo";

    }

    public function CalcularVolumen(): float{
        $r = $this->ancho * $this->alto * $this->largo; 
        return $r;
    }
}







?>
