<?php

abstract class transporte{ //cramos una clase abstracta

    abstract public function Mantenimiento();//creamos un metodo llamado mantenimiento con su modificador de acceso
    //este metodo se declara, pero no se implementa, no tiene funcion en si, 
    //pero obliga que cualquier clase que tenga herencia exista 

}

class Avion extends transporte{//creamos una clase herencia que implemente la clase transpote 

    public function Mantenimiento(){
        echo "<h1> Avion </h1>";
        echo "revision de alas <br>"; 
    } 

}

class Automovil extends transporte{

    public function Mantenimiento(){
        echo "<h1> Automovil </h1>";
        echo "revision de motor <br>";
    }
}


class Tren extends transporte{

    public function Mantenimiento(){
        echo "<h1> Tren </h1>";
        echo "Revision de vias <br>";
        echo "Revision de vagones <br>";
        echo "Revision de controles de maquinaria <br>";
        echo "Revision de frenos";
    }
}

class Motocicleta extends transporte{

    public function Mantenimiento(){
        echo "<h1> Motocicleta </h1>";
        echo "Revision de nivel de gasolina <br>";
        echo "Revision de cadena <br>";
        echo "Revision de aire en los neumaticos <br>";
        echo "Revision de pedales <br>";
    }
}

class Triciclo extends transporte{
    public function Mantenimiento(){
        echo "<h1> Triciclo </h1>";
        echo "Revision de pedales <br>";
        echo "Revision de cadena <br>";
        echo "Revision de rines <br>";
        echo "Revision de aire en los neumaticos <br>";
        echo "Revision de aciento <br>";
    }
}

$obj = new Avion();
$obj-> Mantenimiento();

$obj2 = new Automovil();
$obj2->Mantenimiento(); 

$obj3 = new Tren();
$obj3->Mantenimiento();

$obj4 = new Motocicleta();
$obj4->Mantenimiento();

$obj5 = new Triciclo();
$obj5->Mantenimiento();


?>